package com.ats.executor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.ats.executor.drivers.DriverProcess;

public class StreamGobbler extends Thread
{
	InputStream is;
	String type;
	DriverProcess driverProcess;

	public StreamGobbler(InputStream is, String type, DriverProcess dp)
	{
		this.is = is;
		this.type = type;
		this.driverProcess = dp;
	}

	@Override
	public void run()
	{
		try
		{
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				driverProcess.output(line);	
			}
			
		} catch (IOException ioe)
		{
			ioe.printStackTrace();  
		}
	}
}