/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*/

package com.ats.element;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.ats.generator.variables.CalculatedProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AtsMobileElement extends AtsBaseElement {
	
	private static final String INNER_TEXT = "innerText";
	private static final String TEXT = "text";
	private static final String EMPTY_TAG = "Element";
	private static final String ROOT = "root";

	private boolean clickable = false;

	private AtsMobileElement parent;
	private AtsMobileElement[] children;
	private String positionInDom;

	private Rectangle rect;
	public Rectangle getRect() {
		if (rect == null) {
			rect = new Rectangle(getX().intValue(), getY().intValue(), getWidth().intValue(), getHeight().intValue());
		}
		return rect;
	}
	
	public AtsMobileElement() {
	}
				
	public AtsMobileElement(String id, String tag, Double width, Double height, Double x, Double y, Boolean clickable, Map<String, String> attributes) {
		setId(id);
		setTag(tag);
		setWidth(width);
		setHeight(height);
		setX(x);
		setY(y);
		setAttributes(attributes);
		
		this.clickable = clickable;
	}

	@Override
	public void setTag(String value) {
		if(value == null || value.isEmpty()) {
			super.setTag(EMPTY_TAG);
		}else {
			super.setTag(value);
		}
	}
	
	public void setPositionInDom(String value) {
		this.positionInDom = value;
	}
	
	public String getPositionInDom() {
		return this.positionInDom;
	}
		
	public boolean isClickable() {
		return clickable;
	}

	public void setClickable(boolean clickable) {
		this.clickable = clickable;
	}

	private String getText() {
		final String result = getAttribute(TEXT);
		if(result == null) {
			return "";
		}
		return result;
	}

	private String getInnerText() {
		String result = getText();
		if(children != null) {
			for(int i=0; i<children.length; i++) {
				result += " " + children[i].getInnerText();
			}
		}
		return result.trim();
	}

	public CalculatedProperty[] getMobileAttributes() {
		final List<CalculatedProperty> properties = getProperties();
		properties.add(new CalculatedProperty(INNER_TEXT, getInnerText()));
		return properties.toArray(new CalculatedProperty[properties.size()]);
	}
	
	@Override
	public String getAttribute(String key) {
		if(INNER_TEXT.equals(key)) {
			return getInnerText();
		}
		return super.getAttribute(key);
	}

	public boolean isRoot() {
		return ROOT.equals(getTag());
	}

	public boolean checkTag(String value) {
		return SearchedElement.WILD_CHAR.equals(value) || getTag().toLowerCase().equals(value.toLowerCase());
	}

	public FoundElement getFoundElement() {
		return new FoundElement(this);
	}

	public AtsMobileElement getParent() {
		return parent;
	}

	public void setParent(AtsMobileElement parent) {
		this.parent = parent;
	}

	public AtsMobileElement[] getChildren() {
		if(this.children == null) {
			return new AtsMobileElement[0];
		}
		return children;
	}

	public void setChildren(AtsMobileElement[] children) {
		this.children = children;
	}
	
	public void addChildren(AtsMobileElement element) {
		ArrayList<AtsMobileElement> tmpChild = new ArrayList<AtsMobileElement>();
		if(this.children != null) {
			tmpChild = new ArrayList<AtsMobileElement>(Arrays.asList(this.children));
		}
		tmpChild.add(element);
		this.children = tmpChild.toArray(new AtsMobileElement[tmpChild.size()]);
	}
}