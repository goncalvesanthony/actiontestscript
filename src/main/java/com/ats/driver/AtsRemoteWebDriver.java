/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */

package com.ats.driver;

import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.ats.data.Dimension;
import com.ats.data.Point;

public class AtsRemoteWebDriver extends RemoteWebDriver {

	public AtsRemoteWebDriver() {
	}
	
	public AtsRemoteWebDriver(URL driverServerUrl, DesiredCapabilities capabilities) {
		super(driverServerUrl, capabilities);
	}

	public AtsRemoteWebDriver(URL driverServerUrl, Capabilities cap) {
		super(driverServerUrl, cap);
	}

	public void setWindowSize(Dimension dim) {
		manage().window().setSize(new org.openqa.selenium.Dimension(dim.width, dim.height));
	}
	
	public void setWindowPosition(Point pt) {
		manage().window().setPosition(new org.openqa.selenium.Point(pt.x, pt.y));
	}
}
